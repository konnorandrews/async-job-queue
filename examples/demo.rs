use std::{sync::Arc, time::Duration};

use async_job_queue::{BasicJob, BasicKeeper, JobQueue};
use reqwest::Client;

#[tokio::main]
async fn main() {
    let client = Client::new();
    let (queue, mut dispatch) = JobQueue::new(client, BasicKeeper, 10, 10);

    let handle = tokio::spawn(queue);

    for _ in 0..5 {
        // Wait some time between jobs.
        tokio::time::sleep(Duration::from_millis(5)).await;

        dispatch
            .submit(BasicJob::new_allow_merge((), |client: &Arc<Client>| {
                println!("Started job!");
                let client = client.clone();

                async move {
                    println!("Send request!");

                    let resp = client
                        .get("https://api.sampleapis.com/beers/ale")
                        .send()
                        .await
                        .unwrap();

                    println!("{}", &resp.text().await.unwrap()[..100]);
                }
            }))
            .await
            .unwrap();
    }

    dispatch.shutdown();

    handle.await.unwrap();
}
