use futures::future::BoxFuture;

use crate::Tagged;

pub struct QueueState<'a, C, T> {
    pub max_waiting_count: usize,
    pub receiver_closed: bool,
    pub context: &'a C,
    pub waiting: Vec<&'a mut dyn Tagged<Tag = T>>,
    pub running: Vec<&'a mut dyn Tagged<Tag = T>>,
}

pub struct BasicKeeper;

impl<C> Gatekeeper<C> for BasicKeeper {
    type Tag = ();
    type Output = ();

    fn shutdown(&mut self) -> Self::Output {}
}

impl<T> CleanupJobFinish<T> for BasicKeeper {}
impl<T> CleanupJobCancel<T> for BasicKeeper {}
impl<T> CleanupJobAbort<T> for BasicKeeper {}

pub trait CleanupJobAbort<T> {
    fn abort_job(&mut self, _abort: T) {}
}

pub trait CleanupJobCancel<T> {
    fn cancel_job(&mut self, _cancel: T) {}
}

pub trait CleanupJobFinish<T> {
    fn finish_job(&mut self, _output: T) {}
}

pub trait Gatekeeper<C> {
    /// Extra tag applied to all jobs.
    ///
    /// This is useful for storing keeper specific metadata.
    type Tag: Send + 'static;

    /// The output value of the job queue future.
    type Output;

    fn should_generate_waiting_vec(&self) -> bool {
        false
    }
    fn should_generate_running_vec(&self) -> bool {
        false
    }

    fn update(&mut self, _state: QueueState<C, Self::Tag>) -> ControlFlow {
        ControlFlow::Run
    }
    fn wakeup_future(&mut self) -> Option<BoxFuture<'static, ()>> {
        None
    }
    fn shutdown(&mut self) -> Self::Output;

    fn waiting_job_action(&mut self, _job: &dyn Tagged<Tag = Self::Tag>) -> JobAction {
        JobAction::Start
    }

    fn should_abort_running_job(&self, _job: &dyn Tagged<Tag = Self::Tag>) -> bool {
        false
    }
}

pub enum JobAction {
    Start,
    Cancel,
    Wait,
}

pub enum ControlFlow {
    Run,
    Shutdown,
}
