use std::{any::Any, hash::Hasher};

pub trait OpaqueEq: Any {
    fn hash(&self, state: &mut dyn Hasher);
    fn eq(&self, other: &dyn OpaqueEq) -> bool;
}

impl std::hash::Hash for dyn OpaqueEq {
    fn hash<H: Hasher>(&self, state: &mut H) {
        OpaqueEq::hash(self, state)
    }
}

impl std::cmp::PartialEq for dyn OpaqueEq {
    fn eq(&self, other: &Self) -> bool {
        OpaqueEq::eq(self, other)
    }
}

impl std::cmp::Eq for dyn OpaqueEq {}

impl<T: std::hash::Hash + Eq + Any> OpaqueEq for T {
    fn hash(&self, mut state: &mut dyn Hasher) {
        std::hash::Hash::hash(self, &mut state);
    }

    fn eq(&self, other: &dyn OpaqueEq) -> bool {
        if self.type_id() == other.type_id() {
            let other = unsafe { &*(other as *const dyn OpaqueEq as *const T) };
            self == other
        } else {
            false
        }
    }
}

#[cfg(test)]
mod test {
    use std::collections::hash_map::DefaultHasher;

    use super::*;

    #[test]
    fn opque_eq_eq_works() {
        let a: &dyn OpaqueEq = &123i32;
        let b: &dyn OpaqueEq = &"string";
        let c: &dyn OpaqueEq = &32i32;
        let d: &dyn OpaqueEq = &123i32;

        assert!(a == a);
        assert!(b == b);
        assert!(c == c);
        assert!(d == d);

        assert!(a != b);
        assert!(a != c);
        assert!(a == d);

        assert!(b != a);
        assert!(b != c);
        assert!(b != d);

        assert!(c != a);
        assert!(c != b);
        assert!(c != d);
    }

    fn calc_hash<T: std::hash::Hash>(t: &T) -> u64 {
        let mut s = DefaultHasher::new();
        t.hash(&mut s);
        s.finish()
    }

    #[test]
    fn opque_eq_hash_works() {
        let a: &dyn OpaqueEq = &123i32;
        let b: &dyn OpaqueEq = &"string";
        let d: &dyn OpaqueEq = &123i32;

        assert!(calc_hash(&a) == calc_hash(&a));
        assert!(calc_hash(&b) == calc_hash(&b));
        assert!(calc_hash(&a) == calc_hash(&d));
    }
}
