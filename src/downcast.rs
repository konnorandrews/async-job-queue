use std::any::{Any, TypeId};

use crate::Job;

pub trait Downcast<T: Any>: Any {
    fn is(&self) -> bool;
    fn downcast_ref(&self) -> Option<&T>;
    fn downcast(self: Box<Self>) -> Result<Box<T>, Box<Self>>;
}

impl<T: Any, U: Any> Downcast<T> for U {
    fn is(&self) -> bool {
        let t = TypeId::of::<T>();
        let concrete = Self::type_id(self);
        t == concrete
    }

    fn downcast_ref(&self) -> Option<&T> {
        if Downcast::<T>::is(self) {
            unsafe { Some(downcast_ref_unchecked::<T, Self>(self)) }
        } else {
            None
        }
    }

    fn downcast(self: Box<Self>) -> Result<Box<T>, Box<Self>> {
        if Downcast::<T>::is(&*self) {
            unsafe { Ok(downcast_unchecked::<T, Self>(self)) }
        } else {
            Err(self)
        }
    }
}

impl<I: 'static, D: 'static, T: Any> Downcast<T> for dyn Job<I, D> {
    fn is(&self) -> bool {
        let t = TypeId::of::<T>();
        let concrete = Self::type_id(self);
        t == concrete
    }

    fn downcast_ref(&self) -> Option<&T> {
        if Downcast::<T>::is(self) {
            unsafe { Some(downcast_ref_unchecked::<T, Self>(self)) }
        } else {
            None
        }
    }

    fn downcast(self: Box<Self>) -> Result<Box<T>, Box<Self>> {
        if Downcast::<T>::is(&*self) {
            unsafe { Ok(downcast_unchecked::<T, Self>(self)) }
        } else {
            Err(self)
        }
    }
}

pub(crate) unsafe fn downcast_ref_unchecked<T: Any, U: Downcast<T> + ?Sized>(this: &U) -> &T {
    debug_assert!(Downcast::<T>::is(this));
    unsafe { &*(this as *const U as *const T) }
}

pub(crate) unsafe fn downcast_unchecked<T: Any, U: Downcast<T> + ?Sized>(this: Box<U>) -> Box<T> {
    debug_assert!(Downcast::<T>::is(&*this));
    unsafe {
        let raw: *mut U = Box::into_raw(this);
        Box::from_raw(raw as *mut T)
    }
}
