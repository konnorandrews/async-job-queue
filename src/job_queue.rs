use std::{
    ops::Deref,
    pin::Pin,
    sync::Arc,
    task::{Context, Poll},
};

use futures::{channel::mpsc, future::BoxFuture, FutureExt, SinkExt, StreamExt};
use std::future::Future;

use crate::{
    gatekeeper::{Gatekeeper, QueueState},
    job::Job,
    CleanupJobAbort, CleanupJobCancel, CleanupJobFinish, ControlFlow, JobAction, RunningJob,
};

type DynJob<C, T, Output, Cancel, Abort> =
    dyn Job<Context = C, Tag = T, Output = Output, Cancel = Cancel, Abort = Abort>;

type DynRunningJob<T, Output, Abort> = dyn RunningJob<Tag = T, Output = Output, Abort = Abort>;

#[derive(Clone)]
pub struct Dispatch<C, T, Output, Cancel, Abort> {
    sender: mpsc::Sender<Box<DynJob<C, T, Output, Cancel, Abort>>>,
}

pub struct JobQueue<C, G: Gatekeeper<C>, Output, Cancel, Abort> {
    /// Shared context for the jobs.
    context: Arc<C>,

    /// The gatekeeper decides which jobs to run.
    gatekeeper: G,

    /// Receiver for jobs from the proxy.
    receiver: mpsc::Receiver<Box<DynJob<C, G::Tag, Output, Cancel, Abort>>>,
    receiver_closed: bool,

    /// The jobs waiting to be run.
    waiting: Vec<Option<Box<DynJob<C, G::Tag, Output, Cancel, Abort>>>>,

    /// The max number of waiting jobs allowed.
    max_waiting_count: usize,

    /// The running jobs.
    running: Vec<Option<Box<DynRunningJob<G::Tag, Output, Abort>>>>,

    /// Future to allow the gatekeeper to request sleeping between selecting jobs.
    gatekeeper_sleep_future: Option<BoxFuture<'static, ()>>,

    running_changed: bool,
}

impl<C, G: Gatekeeper<C>, Output, Cancel, Abort> Unpin for JobQueue<C, G, Output, Cancel, Abort> {}

impl<C, G: Gatekeeper<C>, Output, Cancel, Abort> JobQueue<C, G, Output, Cancel, Abort> {
    pub fn new(
        context: C,
        gatekeeper: G,
        channel_size: usize,
        max_waiting_count: usize,
    ) -> (Self, Dispatch<C, G::Tag, Output, Cancel, Abort>) {
        let (sender, receiver) = mpsc::channel(channel_size);

        (
            Self {
                context: Arc::new(context),
                gatekeeper,
                receiver,
                waiting: Vec::new(),
                max_waiting_count,
                running: Vec::new(),
                receiver_closed: false,
                gatekeeper_sleep_future: None,
                running_changed: false,
            },
            Dispatch { sender },
        )
    }
}

impl<C: 'static, G: Gatekeeper<C>, Output: 'static, Cancel: 'static, Abort: 'static> Future
    for JobQueue<C, G, Output, Cancel, Abort>
where
    G: CleanupJobCancel<Cancel> + CleanupJobAbort<Abort> + CleanupJobFinish<Output>,
{
    type Output = G::Output;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        // Forward a change notice from last poll if the running jobs changed.
        let mut changed = this.running_changed;

        // Check for new jobs.
        // This will merge jobs in the waiting queue that are the same together.
        let mut recv_count = 0;
        'recv: while !this.receiver_closed {
            // Stop if no more spots in the waiting queue or we do a lot of receives.
            // We stop after a lot of receives to not starve the running jobs.
            if this.waiting.len() >= this.max_waiting_count || recv_count >= this.max_waiting_count
            {
                // We need to poll again because there may be more jobs in the queue.
                cx.waker().wake_by_ref();
                break;
            }

            match this.receiver.poll_next_unpin(cx) {
                Poll::Ready(Some(job)) => {
                    // A new job was received, the gatekeeper should be run.
                    changed = true;

                    recv_count += 1;

                    let mut job = Some(job);

                    // Try to merge the new job with a running one so its done the
                    // fastest.
                    if job.as_ref().unwrap().consider_merge_with_running() {
                        for existing in &mut this.running {
                            match job
                                .take()
                                .unwrap()
                                .try_merge_with_running(&mut **existing.as_mut().unwrap())
                            {
                                Ok(()) => continue 'recv,
                                Err(j) => job = Some(j),
                            }
                        }
                    }

                    // Try merging the new job with a waiting one.
                    if job.as_ref().unwrap().consider_merge_with_waiting() {
                        for existing in &mut this.waiting {
                            match job
                                .take()
                                .unwrap()
                                .try_merge_with_waiting(&mut **existing.as_mut().unwrap())
                            {
                                Ok(()) => continue 'recv,
                                Err(j) => job = Some(j),
                            }
                        }
                    }

                    // Couldn't merge the job with an existing one, just add it to the
                    // waiting queue.
                    this.waiting.push(job);
                }
                Poll::Ready(None) => this.receiver_closed = true,
                Poll::Pending => break,
            }
        }

        // Check if the gatekeeper wants to do updates.
        if let Some(future) = &mut this.gatekeeper_sleep_future {
            if matches!(future.poll_unpin(cx), Poll::Ready(())) {
                changed = true;

                // The gatekeeper will sleep if it doesn't set a new future later.
                this.gatekeeper_sleep_future = None
            }
        }

        if changed {
            let state = QueueState {
                max_waiting_count: this.max_waiting_count,
                receiver_closed: this.receiver_closed,
                context: this.context.deref(),
                waiting: if this.gatekeeper.should_generate_waiting_vec() {
                    this.waiting
                        .iter_mut()
                        .map(|job| job.as_mut().unwrap().as_tagged_mut())
                        .collect()
                } else {
                    Vec::new()
                },
                running: if this.gatekeeper.should_generate_running_vec() {
                    this.running
                        .iter_mut()
                        .map(|job| job.as_mut().unwrap().as_tagged_mut())
                        .collect()
                } else {
                    Vec::new()
                },
            };
            if matches!(this.gatekeeper.update(state), ControlFlow::Shutdown) {
                // Cancel all waiting jobs.
                for job in this.waiting.drain(..) {
                    this.gatekeeper.cancel_job(job.unwrap().cancel());
                }

                // Abort all running jobs.
                for job in this.running.drain(..) {
                    this.gatekeeper.abort_job(job.unwrap().abort());
                }

                // Shutdown the gatekeeper.
                return Poll::Ready(this.gatekeeper.shutdown());
            }

            // Ask the gatekeeper what to do about the waiting jobs.
            this.waiting.retain_mut(|job| {
                match this
                    .gatekeeper
                    .waiting_job_action(job.as_mut().unwrap().as_tagged_mut())
                {
                    JobAction::Start => {
                        // Promote the job to a running job.
                        let job = job.take().unwrap();
                        this.running.push(Some(job.start(&this.context)));
                        false
                    }
                    JobAction::Cancel => {
                        // Cancel the job.
                        let job = job.take().unwrap();
                        this.gatekeeper.cancel_job(job.cancel());
                        false
                    }
                    JobAction::Wait => true,
                }
            });

            // Ask the gatekeeper what to do about the running jobs.
            this.running.retain_mut(|job| {
                if this
                    .gatekeeper
                    .should_abort_running_job(job.as_mut().unwrap().as_tagged_mut())
                {
                    // Abort the job.
                    let job = job.take().unwrap();
                    this.gatekeeper.abort_job(job.abort());
                    false
                } else {
                    true
                }
            });
        }

        // Poll the running futures, and remove the finished ones.
        this.running_changed = false;
        this.running.retain_mut(|job| {
            match job.as_mut().unwrap().poll(cx) {
                Poll::Ready(()) => {
                    this.running_changed = true;

                    // The job is done, finish it.
                    let job = job.take().unwrap();
                    this.gatekeeper.finish_job(job.finish());
                    false
                }
                Poll::Pending => true,
            }
        });

        // Check if the queue is empty and won't get more jobs.
        if this.receiver_closed && this.waiting.is_empty() && this.running.is_empty() {
            Poll::Ready(this.gatekeeper.shutdown())
        } else {
            Poll::Pending
        }
    }
}

impl<C, T, Output, Cancel, Abort> Dispatch<C, T, Output, Cancel, Abort> {
    pub async fn submit<
        J: Job<Context = C, Tag = T, Output = Output, Cancel = Cancel, Abort = Abort>,
    >(
        &mut self,
        job: J,
    ) -> Result<(), mpsc::SendError> {
        self.sender.send(Box::new(job)).await
    }

    pub fn shutdown(&mut self) {
        self.sender.close_channel();
    }
}

#[cfg(test)]
mod test {
    use std::sync::atomic::{AtomicBool, Ordering};

    use futures::executor::block_on;

    use super::*;

    use crate::{gatekeeper::BasicKeeper, job::BasicJob, mock_context::{MockContext, Yield}};

    #[test]
    fn empty_queue_doesnt_wake() {
        let mock = MockContext::new();

        let (mut queue, _) = JobQueue::<_, _, (), (), ()>::new((), BasicKeeper, 1, 1);

        mock.with_context(|mut cx| {
            assert_eq!(queue.poll_unpin(&mut cx), Poll::Ready(()));
            assert!(!mock.flag());
        });
    }

    #[test]
    fn can_submit_a_job_to_the_queue() {
        let mock = MockContext::new();
        let flag = Arc::new(AtomicBool::new(false));

        let (mut queue, mut proxy) = JobQueue::new((), BasicKeeper, 1, 2);

        mock.with_context(|mut cx| {
            // The queue future won't end yet because the proxy is alive.
            assert_eq!(queue.poll_unpin(&mut cx), Poll::Pending);

            // The future should be sleeping now.
            mock.assert_not_waked();

            // Send a job to the queue.
            block_on(proxy.submit(BasicJob::new((), {
                let flag = flag.clone();
                |_| async move {
                    // Signal that the job was executed.
                    flag.store(true, Ordering::Relaxed);
                }
            })))
            .unwrap();

            // By sending the job it triggers a wakeup.
            mock.assert_waked();

            // Should receive the job, add it to the wait queue, then the gatekeeper
            // should set it to start, then it will finish.
            assert!(!flag.load(Ordering::Relaxed));
            assert_eq!(queue.poll_unpin(&mut cx), Poll::Pending);
            assert_eq!(queue.waiting.len(), 0);
            assert_eq!(queue.running.len(), 0);
            assert!(flag.load(Ordering::Relaxed));

            // All jobs are done so the future should be sleeping again.
            mock.assert_not_waked();

            // Polling with no jobs should do nothing.
            assert_eq!(queue.poll_unpin(&mut cx), Poll::Pending);
            assert_eq!(queue.waiting.len(), 0);
            assert_eq!(queue.running.len(), 0);

            // All jobs are done so the future should be sleeping again.
            mock.assert_not_waked();

            // Dropping the proxy will trigger a wake.
            drop(proxy);
            mock.assert_waked();

            // The queue is now empty and without a proxy so it will end.
            assert_eq!(queue.poll_unpin(&mut cx), Poll::Ready(()));
            mock.assert_not_waked();
        });
    }

    #[test]
    fn can_submit_two_jobs_to_the_queue() {
        let mock = MockContext::new();
        let flag1 = Arc::new(AtomicBool::new(false));
        let flag2 = Arc::new(AtomicBool::new(false));

        let (mut queue, mut proxy) = JobQueue::new((), BasicKeeper, 2, 3);

        mock.with_context(|mut cx| {
            let _ = queue.poll_unpin(&mut cx);

            // Send a job to the queue.
            block_on(proxy.submit(BasicJob::new((), {
                let flag = flag1.clone();
                |_| async move {
                    // Signal that the job was executed.
                    flag.store(true, Ordering::Relaxed);
                }
            })))
            .unwrap();

            // Send a job to the queue.
            block_on(proxy.submit(BasicJob::new((), {
                let flag = flag2.clone();
                |_| async move {
                    // Signal that the job was executed.
                    flag.store(true, Ordering::Relaxed);
                }
            })))
            .unwrap();

            // By sending the job it triggers a wakeup.
            mock.assert_waked();

            // The submitted jobs should finish immediately.
            assert!(!flag1.load(Ordering::Relaxed));
            assert!(!flag2.load(Ordering::Relaxed));
            assert_eq!(queue.poll_unpin(&mut cx), Poll::Pending);
            assert_eq!(queue.running.len(), 0);
            assert_eq!(queue.waiting.len(), 0);
            assert!(flag1.load(Ordering::Relaxed));
            assert!(flag2.load(Ordering::Relaxed));

            // A wake is triggered because we need to start running the new future.
            mock.assert_not_waked();
        });
    }

    #[test]
    fn can_submit_a_long_running_job() {
        let mock = MockContext::new();
        let flag = Arc::new(AtomicBool::new(false));

        let (mut queue, mut proxy) = JobQueue::new((), BasicKeeper, 1, 2);

        mock.with_context(|mut cx| {
            // Send a job to the queue.
            block_on(proxy.submit(BasicJob::new((), {
                let flag = flag.clone();
                |_| async move {
                    for _ in 0..5 {
                        Yield::new().await
                    }

                    // Signal that the job was executed.
                    flag.store(true, Ordering::Relaxed);
                }
            })))
            .unwrap();

            for _ in 0..5 {
                // Polling will start the job.
                assert_eq!(queue.poll_unpin(&mut cx), Poll::Pending);
                assert_eq!(queue.waiting.len(), 0);
                assert_eq!(queue.running.len(), 1);
                assert!(!flag.load(Ordering::Relaxed));

                // The yield causes a wake.
                mock.assert_waked();
            }

            // Polling the last time will stop the job.
            assert_eq!(queue.poll_unpin(&mut cx), Poll::Pending);
            assert_eq!(queue.waiting.len(), 0);
            assert_eq!(queue.running.len(), 0);
            assert!(flag.load(Ordering::Relaxed));

            // All jobs are done so the future should be sleeping again.
            mock.assert_not_waked();
        });
    }
}
