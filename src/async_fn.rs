use futures::future::BoxFuture;
use futures::FutureExt;
use std::any::TypeId;
use std::future::Future;

pub trait AsyncFnOnce<'a, I, O> {
    #[must_use]
    fn execute(self: Box<Self>, input: I) -> BoxFuture<'a, O>;

    fn eq(&self, other: &dyn AsyncFnOnce<'a, I, O>) -> bool {
        self.fn_type_id() == other.fn_type_id()
    }

    fn fn_type_id(&self) -> TypeId;
}

impl<'a, I, O, F, A> AsyncFnOnce<'a, I, O> for F
where
    F: FnOnce(I) -> A + 'static,
    A: Send + Future<Output = O> + 'a,
{
    fn execute(self: Box<Self>, input: I) -> BoxFuture<'a, O> {
        self(input).boxed()
    }

    fn fn_type_id(&self) -> TypeId {
        TypeId::of::<Self>()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn job_can_be_made_from_closure_and_fn_item() {
        let _: &dyn AsyncFnOnce<i32, i32> = &|x| async move { x + 1 };

        async fn a(x: &mut i32) -> &mut i32 {
            *x += 1;
            x
        }

        let _: &dyn AsyncFnOnce<_, _> = &a;
    }

    #[test]
    fn future_can_borrow_from_the_input() {
        async fn a(x: &mut i32) -> &mut i32 {
            b(x).await;
            x
        }

        async fn b(x: &mut i32) {
            *x += 1;
        }

        let y: Box<dyn AsyncFnOnce<_, _>> = Box::new(a);
        let mut x = 42;
        let z = futures::executor::block_on(y.execute(&mut x));
        *z += 1;
        assert_eq!(x, 44);
    }
}
