use std::{
    any::Any,
    pin::Pin,
    sync::Arc,
    task::{Context, Poll},
};

use futures::{future::BoxFuture, Future, FutureExt};

pub trait Tagged: Any {
    type Tag: 'static;

    fn tag(&self) -> &Self::Tag;
    fn tag_mut(&mut self) -> &mut Self::Tag;

    fn as_any_mut(&mut self) -> &mut dyn Any;
}

/// Trait describing a running job.
pub trait RunningJob: Tagged + Send {
    /// The type of the output data given to the [`Gatekeeper`] when
    /// the job is done.
    type Output: 'static;

    /// The type of the output data given to the [`Gatekeeper`] when
    /// the job is aborted.
    type Abort: 'static;

    /// The poll method of the job's future.
    ///
    /// This method drives the job forward. Returning `Ready` signals that the job is done.
    fn poll(&mut self, cx: &mut Context<'_>) -> Poll<()>;

    /// Finish the job and generate the output value.
    fn finish(self: Box<Self>) -> Self::Output;

    fn abort(self: Box<Self>) -> Self::Abort;

    fn as_tagged_mut(&mut self) -> &mut dyn Tagged<Tag = Self::Tag>;
}

/// Trait describing a waiting job.
pub trait Job: Tagged + Send {
    /// Shared context the job is running in.
    type Context: 'static;

    /// The type of the output data given to the [`Gatekeeper`] when
    /// the job is done.
    type Output: 'static;

    /// The type of the output data given to the [`Gatekeeper`] when
    /// the job is not executed.
    type Cancel: 'static;

    /// The type of the output data given to the [`Gatekeeper`] when
    /// the job is aborted.
    type Abort: 'static;

    /// Start the job.
    fn start(self: Box<Self>, context: &Arc<Self::Context>) -> Box<RunningJobFor<Self>>;

    /// Check if the job has been cancelled.
    ///
    /// If a job has been cancelled then it will be aborted.
    fn should_cancel(&self) -> bool;

    /// Called when the job is cancelled.
    fn cancel(self: Box<Self>) -> Self::Cancel;

    /// Check if this job could be merged with an existing waiting job.
    fn consider_merge_with_waiting(&self) -> bool;

    /// Try to merge this job with an existing waiting job.
    ///
    /// The `Err` variant should return `self` if unable to merge.
    fn try_merge_with_waiting(
        self: Box<Self>,
        waiting: &mut SimilarJob<Self>,
    ) -> Result<(), Box<SimilarJob<Self>>>;

    /// Check if this job could be merged with an existing running job.
    fn consider_merge_with_running(&self) -> bool;

    /// Try to merge this job with an existing running job.
    ///
    /// The `Err` variant should return `self` if unable to merge.
    fn try_merge_with_running(
        self: Box<Self>,
        running: &mut RunningJobFor<Self>,
    ) -> Result<(), Box<SimilarJob<Self>>>;

    fn as_tagged_mut(&mut self) -> &mut dyn Tagged<Tag = Self::Tag>;
}

pub type SimilarJob<T> = dyn Job<
    Context = <T as Job>::Context,
    Tag = <T as Tagged>::Tag,
    Output = <T as Job>::Output,
    Cancel = <T as Job>::Cancel,
    Abort = <T as Job>::Abort,
>;

type RunningJobFor<T> = dyn RunningJob<
    Tag = <T as Tagged>::Tag,
    Output = <T as Job>::Output,
    Abort = <T as Job>::Abort,
>;

pub struct BasicJob<C, T> {
    allow_merge: bool,
    fn_once: Box<BasicJobFn<C>>,
    tag: T,
}

type BasicJobFn<C> = dyn for<'a> FnOnce(&'a Arc<C>) -> BoxFuture<'static, ()> + Send;

pub struct BasicRunningJob<T> {
    future: Pin<Box<dyn Future<Output = ()> + Send>>,
    allow_merge: bool,
    tag: T,
}

impl<T> Unpin for BasicRunningJob<T> {}

impl<C, T: Send> BasicJob<C, T> {
    pub fn new<
        F: for<'a> FnOnce(&'a Arc<C>) -> A + Send + 'static,
        A: Future<Output = ()> + Send + 'static,
    >(
        tag: T,
        f: F,
    ) -> Self {
        Self {
            allow_merge: false,
            fn_once: Box::new(move |context| Box::pin(f(context))),
            tag,
        }
    }

    pub fn new_allow_merge<
        F: for<'a> FnOnce(&'a Arc<C>) -> A + Send + 'static,
        A: Future<Output = ()> + Send + 'static,
    >(
        tag: T,
        f: F,
    ) -> Self {
        Self {
            allow_merge: true,
            fn_once: Box::new(move |context| Box::pin(f(context))),
            tag,
        }
    }
}

impl<C: 'static, T: Send + 'static> Tagged for BasicJob<C, T> {
    type Tag = T;

    fn tag(&self) -> &Self::Tag {
        &self.tag
    }

    fn tag_mut(&mut self) -> &mut Self::Tag {
        &mut self.tag
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}

impl<C: 'static, T: Send + 'static> Job for BasicJob<C, T> {
    type Context = C;

    type Output = T;

    type Cancel = T;

    type Abort = T;

    fn start(self: Box<Self>, context: &Arc<Self::Context>) -> Box<RunningJobFor<Self>> {
        let future = (self.fn_once)(context);

        Box::new(BasicRunningJob {
            future,
            allow_merge: self.allow_merge,
            tag: self.tag,
        })
    }

    fn should_cancel(&self) -> bool {
        false
    }

    fn cancel(self: Box<Self>) -> Self::Cancel {
        self.tag
    }

    fn consider_merge_with_waiting(&self) -> bool {
        self.allow_merge
    }

    fn try_merge_with_waiting(
        self: Box<Self>,
        waiting: &mut SimilarJob<Self>,
    ) -> Result<(), Box<SimilarJob<Self>>> {
        if let Some(waiting) = waiting.as_any_mut().downcast_mut::<Self>() {
            if waiting.allow_merge && self.allow_merge {
                // A basic job doesn't need to do anything on a merge.
                Ok(())
            } else {
                Err(self)
            }
        } else {
            Err(self)
        }
    }

    fn consider_merge_with_running(&self) -> bool {
        self.allow_merge
    }

    fn try_merge_with_running(
        self: Box<Self>,
        running: &mut RunningJobFor<Self>,
    ) -> Result<(), Box<SimilarJob<Self>>> {
        if let Some(running) = running.as_any_mut().downcast_mut::<BasicRunningJob<T>>() {
            if running.allow_merge && self.allow_merge {
                // A basic job doesn't need to do anything on a merge.
                Ok(())
            } else {
                Err(self)
            }
        } else {
            Err(self)
        }
    }

    fn as_tagged_mut(&mut self) -> &mut dyn Tagged<Tag = Self::Tag> {
        self
    }
}

impl<T: Send + 'static> RunningJob for BasicRunningJob<T> {
    type Output = T;

    type Abort = T;

    fn poll(&mut self, cx: &mut Context<'_>) -> Poll<()> {
        self.future.poll_unpin(cx)
    }

    fn finish(self: Box<Self>) -> Self::Output {
        self.tag
    }

    fn abort(self: Box<Self>) -> Self::Abort {
        self.tag
    }

    fn as_tagged_mut(&mut self) -> &mut dyn Tagged<Tag = Self::Tag> {
        self
    }
}

impl<T: Send + 'static> Tagged for BasicRunningJob<T> {
    type Tag = T;

    fn tag(&self) -> &Self::Tag {
        &self.tag
    }

    fn tag_mut(&mut self) -> &mut Self::Tag {
        &mut self.tag
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self
    }
}
