use std::{
    sync::{atomic::AtomicBool, Arc},
    task::{Context, Poll, Waker},
};

use futures::{
    task::{waker, ArcWake},
    Future,
};

pub struct MockContext {
    waker: Arc<MockWaker>,
}

struct MockWaker {
    wake_flag: AtomicBool,
}

impl ArcWake for MockWaker {
    fn wake_by_ref(arc_self: &Arc<Self>) {
        arc_self
            .wake_flag
            .store(true, std::sync::atomic::Ordering::Relaxed);
    }
}

impl MockContext {
    pub fn new() -> Self {
        Self {
            waker: Arc::new(MockWaker {
                wake_flag: AtomicBool::new(false),
            }),
        }
    }

    pub fn flag(&self) -> bool {
        self.waker
            .wake_flag
            .load(std::sync::atomic::Ordering::Relaxed)
    }

    pub fn reset(&self) {
        self.waker
            .wake_flag
            .store(false, std::sync::atomic::Ordering::Relaxed);
    }

    pub fn waker(&self) -> Waker {
        waker(self.waker.clone())
    }

    pub fn with_context<T, F>(&self, f: F) -> T
    where
        F: FnOnce(Context) -> T,
    {
        let waker = self.waker();
        let context = Context::from_waker(&waker);
        f(context)
    }

    #[track_caller]
    pub fn assert_waked(&self) {
        assert!(self.flag(), "Waker was not called");
        self.reset();
    }

    #[track_caller]
    pub fn assert_not_waked(&self) {
        assert!(!self.flag(), "Waker was called");
    }
}

pub struct Yield {
    ready: bool,
}

impl Yield {
    pub fn new() -> Self {
        Self { ready: false }
    }
}

impl Future for Yield {
    type Output = ();

    fn poll(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> std::task::Poll<Self::Output> {
        if self.ready {
            Poll::Ready(())
        } else {
            self.ready = true;
            cx.waker().wake_by_ref();
            Poll::Pending
        }
    }
}
